var facebookImageDownloader = {
    initialized: false,
    bindAddClass: '.fb-download.add',
    bindRemoveClass: '.fb-download.remove',
    bindDownloadClass: '.download-fb-images',
    imagesToDownload: [],
    init: function() {
        if (facebookImageDownloader.initialized == false) {
            facebookImageDownloader.initialized = true;
            facebookImageDownloader.bind();
        }
    },
    bind: function () {
        $(document).on('click', facebookImageDownloader.bindAddClass, facebookImageDownloader.addToList);
        $(document).on('click', facebookImageDownloader.bindRemoveClass, facebookImageDownloader.removeFromList);
        $(document).on('click', facebookImageDownloader.bindDownloadClass, facebookImageDownloader.download);
    },
    addToList: function () {
        $(this).removeClass('add');
        $(this).addClass('remove');
        var id = $(this).attr('data-id');
        facebookImageDownloader.imagesToDownload.push(id);
    },
    removeFromList: function () {
        $(this).removeClass('remove');
        $(this).addClass('add');
        var id = $(this).attr('data-id');
        var index = facebookImageDownloader.imagesToDownload.indexOf(id);
        facebookImageDownloader.imagesToDownload.splice(index, 1);
    },
    download: function() {
        var url = $(this).data('url');
        var token = $(this).data('token');

        var data = {'images': facebookImageDownloader.imagesToDownload, _token: token};

        $.ajax({
            type: "POST",
            data: data,
            url: url,
            success: function(msg){
                window.location.reload();
            }
        });

        /*
        $.post(url, data, function (response) {
            //console.log(response);
        });
        */
    }
}

facebookImageDownloader.init();