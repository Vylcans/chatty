var chat = document.getElementById('chat-window');
var msg = document.getElementById('message-box');

var socket = new WebSocket("ws://chatty.lh:9090")

var open = false;

function addMessage (msg, time, name) {

    chat.innerHTML += msg;
}

msg.addEventListener('keypress', function (evt) {
    if (evt.charCode != 13) {
        return;
    }

    evt.preventDefault();

    if (msg.value == "" || !open) {
        return;
    }

    socket.send(JSON.stringify({
        msg: msg.value
    }));

    //addMessage(msg.value);

    msg.value = "";
});

socket.onopen = function () {
    open = true;

    addMessage("Connected");
};

socket.onmessage = function (evt) {
    var data = JSON.parse(evt.data);
    addMessage(data.msg);
};

socket.onclose = function () {
    open = false;

    addMessage("Connection closed. <br> To start: artisan chat:serve");
};