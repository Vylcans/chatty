<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sociālais Tīkls</title>
    <meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no" />
    <link rel="icon" href="images/favicon.ico">
    <link rel="shortcut icon" href="images/favicon.ico" />
    <link rel="stylesheet" href="{{ asset('landingPage/css/contact-form.css') }}">
    <link rel="stylesheet" href="{{ asset('landingPage/css/touchTouch.css') }}">
    <link rel="stylesheet" href="{{ asset('landingPage/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('landingPage/css/style.css') }}">
    <script src="{{ asset('/landingPage/js/jquery.js') }}"></script>
    <script src="{{ asset('/landingPage/js/jquery-migrate-1.1.1.js') }}"></script>
    <script src="{{ asset('/landingPage/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('/landingPage/js/script.js') }}"></script>
    <script src="{{ asset('/landingPage/js/superfish.js') }}"></script>
    <script src="{{ asset('/landingPage/js/jquery.equalheights.js') }}"></script>
    <script src="{{ asset('/landingPage/js/jquery.mobilemenu.js') }}"></script>
    <script src="{{ asset('/landingPage/js/jquery.ui.totop.js') }}"></script>
    <script src="{{ asset('/landingPage/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('/landingPage/js/touchTouch.jquery.js') }}"></script>

    <script src="{{ asset('/landingPage/js/modal.js') }}"></script>

    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/html5shiv.js') }}"></script>
    <link rel="stylesheet" media="screen" href="css/ie.css">
    <![endif]-->
</head>

<body>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '519404098240506',
            xfbml      : true,
            version    : 'v2.5'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!--==============================
              header
=================================-->
<div class="menu_bg"></div>
<header>
    <div class="navigation single-page-nav">
        <div class="container_12">
            <div class="grid_12">
                <h1 class="logo">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('landingPage/images/logo.png') }}" alt="">
                    </a>
                </h1>
                <nav>
                    <ul>
                        <li><a href="#cause" class="current">Mērķis</a></li>
                        <li><a href="#technology">Tehnoloģijas</a></li>
                        <li><a href="#database">Datubāze</a></li>
                        <li><a href="#registration">Reģistrēties</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="container_12">
        <div class="grid_12 ">
            <h2>Šis ir testa sociālais tīkls<br>
                <span></span>
            </h2>
            <div>
                <form class="grid_4 landing-form prefix_4" action="{{ route('auth.signin') }}" method="post">
                    <input type="text" name="email" />
                    <input type="password" name="password" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" value="Ienākt" class="btn" />
                </form>
            </div>
        </div>
        <div>
            <a href="{{ $facebookLoginUrl }}" class="btn">Ienākt ar Facebook</a>
        </div>
        <div class="clear"></div>
    </div>
</header>
<!--=====================
          Content
======================-->
<section id="cause" class="page">
    <div class="container_12"><div class="ic">More Website Templates @ TemplateMonster.com - August26, 2014!</div>
        <div class="grid_12">
            <h3>Projekta mērķis</h3>
        </div>
        <div class="grid_12">
            Projekta mērķis ir apgūt izstrādājot tehnoloģijas, kā arī demonstrēt apgūtās tehnoloģijas.
        </div>
        <div class="clear"></div>
    </div>
</section>
<section id="technology" class="page page_bg__1">
    <div class="container_12"><div class="ic">More Website Templates @ TemplateMonster.com - August26, 2014!</div>
        <div class="grid_12">
            <h3>Izmantotās tehnoloģijas</h3>
        </div>
        <div class="grid_9">
            <div class="serv_links">
                <a href="#" class="serv"><span>WebSocket</span></a>
                <a href="#" class="serv"><span>MySql</span></a>
                <a href="#" class="serv"><span>Laravel</span></a>
                <a href="#" class="serv"><span>Twitter<br />Bootstrap</span></a>
                <a href="#" class="serv"><span>Git</span></a>
            </div>
        </div>
        <div class="grid_3 fwn">
            <div class="text1">Projektā izmantotās tehnoloģijas</div>
            Projekts tiek balstīts uz <strong>Laravel 5.0</strong> PHP ietvaru. <strong>MySQL</strong> datubāze nodrošina datu uzglabāšanu.
            Versiju kontrolei tiek izmantota <strong>Git</strong> tehnoloģija <strong>GitHub</strong> servisā.
            Landing page salikums ir balstīts uz template, ko izstrādājis <strong>TemplateMonster.com (linku)</strong>.
            Iekšējās sistēmas lapu salikums tiek balstīts uz <strong>Twitter Bootstrap</strong>. Čatam tiek izmantota <strong>WebSocket</strong> tehnoloģija.
            Ir doma pieslēgt autorizēšanos caur Facebook.
        </div>
        <div class="clear"></div>
    </div>
</section>
<section id="database" class="page">
    <div class="about_inset__1">
        <div class="container_12">
            <div class="grid_12">
                <h3 class="head__1">Datubāze</h3>
                <div class="">
                    <div class="grid_12">
                        <img src="{{ asset('landingPage/images/chatty.png') }}" title="Database schema">
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>      <div class="clear"></div>

        </div>
    </div>
</section>
<section id="registration" class="page">
    <div class="contacts_bg">
        <div class="container_12">
            <div class="grid_12">
                <h3 class="head__1">Reģistrācija</h3>
            </div>
            <div class="grid_7">
                <div class="text2">Reģistrācija</div>
                Bla, bla, bla, ir iespēja reģistrēties, lai notestētu sistēmu
            </div>
            <div class="grid_4 prefix_1">
                <div class="text2">Reģistrēties</div>
                <form class="landing-form" method="post" action="{{ route('auth.signup') }}">
                    <div class="contact-form-loader"></div>
                    <fieldset>
                        <div>
                            <input type="text" autocomplete="off" name="firstName" @if(Input::old('firstName'))value="{{ Input::old('firstName') }}"@endif id="first-name" placeholder="Vārds">
                            @if($errors->has('firstName'))
                                <span class="error-message">
                                    {{ $errors->first('firstName') }}
                                </span>
                            @endif
                        </div>

                        <div>
                            <input type="text" autocomplete="off" name="lastName" @if(Input::old('lastName'))value="{{ Input::old('lastName') }}"@endif id="last-name" placeholder="Uzvārds">
                            @if($errors->has('lastName'))
                                <span class="help-block">
                                    {{ $errors->first('lastName') }}
                                </span>
                            @endif
                        </div>
                        <div>
                            <input type="text" autocomplete="off" name="username" @if(Input::old('username'))value="{{ Input::old('username') }}"@endif id="username" placeholder="Lietotājvārds">
                            @if($errors->has('username'))
                                <span class="help-block">
                                    {{ $errors->first('username') }}
                                </span>
                            @endif
                        </div>

                        <div>
                            <input type="email" autocomplete="off" name="email" @if(Input::old('email'))value="{{ Input::old('email') }}"@endif id="email" placeholder="E-pasts">
                            @if($errors->has('email'))
                                <span class="help-block">
                                    {{ $errors->first('email') }}
                                </span>
                            @endif
                        </div>

                        <div>
                            <input type="password" autocomplete="off" name="password" id="password" placeholder="Parole">
                            @if($errors->has('password'))
                                <span class="help-block">
                                    {{ $errors->first('password') }}
                                </span>
                            @endif
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="ta__right">
                            <input type="submit"class="btn" value="Reģistrēties">
                        </div>
                    </fieldset>
                    <div class="modal fade response-message">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Modal title</h4>
                                </div>
                                <div class="modal-body">
                                    You message has been sent! We will be in touch soon.
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
<!--==============================
              footer
=================================-->
<footer id="footer">
    <div class="container_12">
        <div class="grid_12">
            <div class="copyright"><span class="color1">DSGN</span> &copy; <span id="copyright-year">2014</span> | <a href="#">Privacy Policy </a>
                <div class="sub-copy">Website designed by <a href="http://www.templatemonster.com/" rel="nofollow">TemplateMonster.com</a></div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</footer>
<a href="#" id="toTop" class="fa fa-chevron-up"></a>
<script src="{{ asset('/landingPage/js/jquery.singlePageNav.min.js') }}"></script>
<script>

    // Prevent console.log from generating errors in IE for the purposes of the demo
    if ( ! window.console ) console = { log: function(){} };

    // The actual plugin
    $('.single-page-nav').singlePageNav({
        offset: $('.single-page-nav').outerHeight(),
        filter: ':not(.external)',
        updateHash: true,
        beforeStart: function() {
            console.log('begin scrolling');
        },
        onComplete: function() {
            console.log('done scrolling');
        }
    });
</script>
</body>
</html>

