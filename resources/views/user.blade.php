@extends('app')
@section('content')
    <div class="row">
        <div class="col-lg-5">
            <!-- User information and statuses -->
            <div class="panel panel-primary">
                <div class="panel-heading">Lietotāja informācija</div>
                <div class="panel-body">
                    @include('userBlock', ['user' => $user])
                    @if(Auth::user() != $user)
                        <div class="form-group">
                            @if(Auth::user()->isFriendWith($user))
                                <form action="{{ route('user.unfriend') }}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="userId" value="{{ $user->id }}">
                                    <input type="submit" value="Pārtraukt draudzību" class="btn btn-danger">
                                </form>
                            @elseif(Auth::user()->hasPendingFriendRequestsCreated($user))
                                <form action="{{ route('user.unfriend') }}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="userId" value="{{ $user->id }}">
                                    <input type="submit" value="Atcelt uzaicinājumu" class="btn btn-warning">
                                </form>
                            @elseif(Auth::user()->hasPendingReceivedFriendRequest($user))
                                <a href="{{ route('user.accept', ['id' => $user->id]) }}" class="btn btn-default">Apstiprināt uzaicinājumu</a>
                            @else
                                <a href="{{ route('user.invite', ['id' => $user->id]) }}" class="btn btn-info">Uzaicināt par draugu</a>
                            @endif
                        </div>
                        <div class="form-group">
                            <form method="post" action="{{ route('message.send') }}">
                                <input type="hidden" name="recipientId" value="{{ $user->id }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <textarea rows="3" class="form-control col-lg-12" name="body" placeholder="Vēstules saturs"></textarea>
                                </div>
                                <input type="submit" value="Sūtīt vēstuli" class="btn btn-default">
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-lg-offset-3">
            <!-- Friends, friend requests -->
            <h4>Draugi</h4>
            @if($user->friends()->count() == 0)
                No friends
            @else
                @foreach($user->friends() as $friend)
                    @include('userBlock', ['user' => $friend])
                @endforeach
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div class="panel panel-primary">
                <div class="panel-heading">Laika josla</div>
                <div class="panel-body">
                    @if(!$statuses->count())
                        Pagaidām nav ierakstu
                    @else
                        @foreach($statuses as $status)
                            @include('timelineStatus', ['status' => $status])
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection