@extends('app')
@section('content')
    <div class="row">
        <div class="col-lg-5">
            <h4>{{ Auth::user()->getNameOrUsername() }}</h4>
            @if(is_file(Auth::user()->getAvatarFile(\App\Models\User::AVATAR_THUMBNAIL_PREFIX)))
                <img src="{{ Auth::user()->getAvatarLink(\App\Models\User::AVATAR_THUMBNAIL_PREFIX) }}" class="thumbnail" >
            @endif
            <h4>Saglabātie attēli</h4>
            <div>
                @foreach($images as $userImage)
                    <img src="{{ $userImage->getUserImageSource() }}" class="thumbnail pull-left" width="100" height="100">
                @endforeach
            </div>
        </div>
        <div class="col-lg-6 pull-right">
            <h4>Pieejamie Facebook attēli</h4>
            <div>
                @foreach($facebookImages as $image)
                    <img src="{{ $image['picture'] }}" data-id="{{ $image['id'] }}" class="fb-download thumbnail pull-right @if($image['isLocal'])remove @else add @endif" width="100" height="100">
                @endforeach
            </div>
            <div class="text-right">
                <button class="download-fb-images btn btn-default" data-token="{{ csrf_token() }}" data-url="{{ route('facebook.downloadImages') }}">Lejupielādēt</button>
            </div>
        </div>
    </div>
@endsection
@section('customJs')
    <script src="{{ asset('js/facebookImageDownloader.js') }}"></script>
@endsection