<div class="col-lg-3 pull-right">
    <div class="panel panel-primary">
        <div class="panel-heading">Čats</div>
        <div class="panel-body">
            <div id="chat-window"></div>
            <div class="form">
                <textarea id="message-box" name="message" placeholder="Message:" rows="3" class="col-lg-12"></textarea>
            </div>
        </div>
    </div>
</div>