@extends('app')
@section('customCss')
    <link rel="stylesheet" href="{{ asset('css/public.css') }}">
@endsection
@section('content')
    <div class="row">
        <div class="pull-left col-md-3">
            <img src="" title="Test Social Network" alt="Test Social Network">
        </div>
        <div class="pull-right col-md-5">
            <form class="form-horizontal">
                <input name="email" placeholder="Email">
                <input name="password" placeholder="*********">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" value="Sign In">
            </form>
            <p>
                Don't have an account yet? Sign up!
            </p>
        </div>
    </div>
@endsection