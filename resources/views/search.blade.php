@extends('app')
@section('content')
    <h1>Search</h1>
    <h3>Search results for: {{ $query }}</h3>
    <div class="row">
        @if($result->count() == 0)
            No results found
        @else
            @foreach($result as $user)
                @include('userBlock', ['user' => $user])
            @endforeach
        @endif
    </div>
@endsection