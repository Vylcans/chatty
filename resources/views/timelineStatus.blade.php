<div class="media well">
    <a class="pull-left" href="{{ route('user.usersProfile', ['username' => $status->user->username]) }}">
        <img src="{{ $status->user->getAvatarUrl() }}" class="media-object" alt="">
    </a>
    <div class="media-body">
        <h5 class="media-heading">
            <a href="{{ route('user.usersProfile', ['username' => $status->user->username]) }}">
                {{ $status->user->getNameOrUsername() }}
            </a>
        </h5>
        <p>
            {{ $status->body }}
        </p>
        <ul class="list-inline">
            <li>{{ $status->created_at->diffForHumans() }}</li>
            @if($status->user->id != Auth::user()->id && !Auth::user()->hasLikedStatus($status))
                <li><a href="{{ route('status.like', ['statusId' => $status->id]) }}">Like</a> </li>
            @endif
            <li>{{ $status->likes->count() }} likes</li>
        </ul>

        @foreach($status->replies as $reply)
            <div class="media">
                <a class="pull-left" href="{{ route('user.usersProfile', ['username' => $reply->user->username]) }}">
                    <img src="{{ $reply->user->getAvatarUrl() }}" class="media-object" alt="">
                </a>
                <div class="media-body">
                    <h6 class="media-heading">
                        <a href="#">
                            {{ $reply->user->getNameOrUsername() }}
                        </a>
                    </h6>
                    <p>
                        {{ $reply->body }}
                    </p>
                    <ul class="list-inline">
                        <li>{{ $reply->created_at->diffForHumans() }}</li>
                        @if($reply->user->id != Auth::user()->id && !Auth::user()->hasLikedStatus($reply))
                            <li><a href="{{ route('status.like', ['statusId' => $reply->id]) }}">Like</a> </li>
                        @endif
                        <li>{{ $reply->likes->count() }} likes</li>
                    </ul>
                </div>
            </div>
        @endforeach
        @if(!isset($authUserIsFriend) or (isset($authUserIsFriend) and $authUserIsFriend))
            <form role="form" action="{{ route('status.reply', ['statusId' => $status->id]) }}" method="post">
                <div class="form-group {{ $errors->has('reply-' . $status->id) ? 'has-error' : '' }}">
                    <textarea name="reply-{{ $status->id }}" class="form-control" rows="2" placeholder="Atbildēt"></textarea>
                </div>

                @if($errors->has("reply-" . $status->id))
                    <span class="help-block">
                        {{ $errors->first("reply-" . $status->id) }}
                    </span>
                @endif
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" value="Atbildēt" class="btn btn-default btn-sm">
            </form>
        @endif
    </div>
</div>