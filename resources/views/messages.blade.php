@extends('app')
@section('content')
    <div class="row">
        <div class="col-lg-5">
            <h4>Vēstules</h4>
            @foreach($messages as $message)
                <div class="well">
                    <a role="button" data-toggle="collapse" href="#collapse{{ $message->id }}" aria-expanded="false" aria-controls="collapseExample">
                        {{ $message->sender->getNameOrUsername() }} @if(isset($message->new) and $message->new)<span class="badge">Jauns</span>@endif
                    </a>
                    <div class="collapse" id="collapse{{ $message->id }}">
                        <div class="">
                            <div class="media form-group">
                                <a href="{{ route('user.usersProfile', ['username' => $message->sender->username]) }}" class="pull-left">
                                    <img src="{{ $message->sender->getAvatarUrl() }}" class="media-object">
                                </a>

                                <div class="media-body">
                                    <h4 class="media-heading">
                                        <a href="{{ route('user.usersProfile', ['username' => $message->sender->username]) }}" class="pull-left">
                                            {{ $message->sender->getNameOrUsername() }}
                                        </a>
                                    </h4>
                                </div>
                                <div>
                                    {{ $message->body }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection