@extends('app')
@section('content')
    <div class="row">
        <div class="col-lg-5">
            <div class="panel panel-primary">
                <div class="panel-heading">Lietotāja informācija</div>
                <div class="panel-body">
                    @include('userBlock', ['user' => $user])
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">Kas jauns?</div>
                <div class="panel-body">
                    <form role="form" action="{{ route('status.post') }}" method="post">
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <textarea placeholder="Kas jauns, {{ Auth::user()->getNameOrUsername() }}?" name="status" class="form-control" rows="2"></textarea>
                            @if($errors->has('status'))
                                <span class="help-block">{{ $errors->first('status') }}</span>
                            @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        </div>
                        <button type="submit" class="btn btn-default">Saglabāt</button>
                    </form>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">Laika josla</div>
                <div class="panel-body">
                    @if(!$statuses->count())
                        Pagaidām nav ierakstu
                    @else
                        @foreach($statuses as $status)
                            @include('timelineStatus', ['status' => $status])
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading">Jaunākie draugu attēli</div>
                <div class="panel-body">
                    @foreach($friendImages as $userImage)
                        <a href="{{ route('user.usersProfile', ['username' => $userImage->imaginable->username]) }}">
                            <img src="{{ $userImage->getUserImageSource() }}" class="thumbnail pull-left" width="100" height="100">
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
        @include('chat')
    </div>


@endsection
@section('customJs')
    <script src="{{ asset('js/chat.js') }}"></script>
@endsection