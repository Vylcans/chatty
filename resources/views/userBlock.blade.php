<div class="media form-group">
    <a href="{{ route('user.usersProfile', ['username' => $user->username]) }}" class="pull-left">
        <img src="{{ $user->getAvatarUrl() }}" class="media-object">
    </a>

    <div class="media-body">
        <h4 class="media-heading">
            <a href="{{ route('user.usersProfile', ['username' => $user->username]) }}" class="pull-left">
                {{ $user->getNameOrUsername() }}
            </a>
        </h4>
    </div>
</div>