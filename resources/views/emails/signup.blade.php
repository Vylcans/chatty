Hello, {{ $nameOrUsername }}! <br>
You are registered with email {{ $email }}.<br />
Please login with the password {{ $password }}.