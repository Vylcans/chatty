@extends('app')
@section('content')
    <div class="row">
        <div class="col-lg-5">
            <h4>{{ $user->getNameOrUsername() }}</h4>
            @if(is_file($user->getAvatarFile(\App\Models\User::AVATAR_THUMBNAIL_PREFIX)))
                <img src="{{ $user->getAvatarLink(\App\Models\User::AVATAR_THUMBNAIL_PREFIX) }}" class="thumbnail" >
            @endif
            <form action="{{ route('user.profile') }}" method="post" enctype="multipart/form-data">
                <div class="form-group @if($errors->has('firstName')) has-error @endif">
                    <label for="first-name">Vārds</label>
                    <input type="text" name="firstName" class="form-control" placeholder="Vārds" value="@if(!empty(Input::old('firstName'))){{ Input::old('firstName') }}@else{{ $user->first_name }}@endif">
                    @if($errors->has('firstName'))
                        <div class="error-message">
                            {{ $errors->first('firstName') }}
                        </div>
                    @endif
                </div>

                <div class="form-group @if($errors->has('lastName')) has-error @endif">
                    <label for="last-name">Uzvārds</label>
                    <input type="text" name="lastName" class="form-control" placeholder="Uzvārds" value="@if(!empty(Input::old('lastName'))){{ Input::old('lastName') }}@else{{ $user->last_name }}@endif">
                    @if($errors->has('lastName'))
                        <div class="error-message">
                            {{ $errors->first('lastName') }}
                        </div>
                    @endif
                </div>

                <div class="form-group @if($errors->has('username')) has-error @endif">
                    <label for="username">Lietotājvārds</label>
                    <input type="text" name="username" class="form-control" placeholder="Lietotājvārds" value="@if(!empty(Input::old('username'))){{ Input::old('username') }}@else{{ $user->username }}@endif">
                    @if($errors->has('username'))
                        <div class="error-message">
                            {{ $errors->first('username') }}
                        </div>
                    @endif
                </div>

                <div class="form-group @if($errors->has('email')) has-error @endif">
                    <label for="email">E-pasts</label>
                    <input type="text" name="email" class="form-control" placeholder="E-pasts" value="@if(!empty(Input::old('email'))){{ Input::old('email') }}@else{{ $user->email }}@endif">
                    @if($errors->has('email'))
                        <div class="error-message">
                            {{ $errors->first('email') }}
                        </div>
                    @endif
                </div>

                <div class="form-group @if($errors->has('avatar')) has-error @endif">
                    <label for="avatar">Attēls</label>
                    <input type="file" name="avatar" class="form-control">
                    @if($errors->has('avatar'))
                        <div class="error-message">
                            {{ $errors->first('avatar') }}
                        </div>
                    @endif
                </div>
                
                <div class="form-group">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" class="btn btn-default" value="Saglabāt">
                </div>
            </form>
            @if(!is_null($facebookLoginUrl))
                <a href="{{ $facebookLoginUrl }}" class="btn btn-info">Link Facebook</a>
            @endif
        </div>
        <div class="col-lg-6 pull-right">

        </div>
    </div>
@endsection