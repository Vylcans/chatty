@extends('app')
@section('content')
    <div class="row">
        <div class="col-lg-5">
            <!-- Friends -->
            <h4>Draugi</h4>
            @if($friends->count() == 0)
                No friends
            @else
                @foreach($friends as $friend)
                    @include('userBlock', ['user' => $friend])
                @endforeach
            @endif
        </div>
        <div class="col-lg-4 col-lg-offset-3">
            <!-- Friend requests -->
            <h4>Saņemtie uzaicinājumi</h4>

            @if($friendRequestsReceived->count() == 0)
                Nav saņemto uzaicinājumu
            @else
                @foreach($friendRequestsReceived as $receivedRequest)
                    @include('userBlock', ['user' => $receivedRequest])
                @endforeach
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-lg-offset-8">
            <!-- Friend requests -->
            <h4>Nosūtītie uzaicinājumi</h4>

            @if($friendRequestsCreated->count() == 0)
                Nav nosūtīto uzaicinājumu
            @else
                @foreach($friendRequestsCreated as $createdRequest)
                    @include('userBlock', ['user' => $createdRequest])
                @endforeach
            @endif
        </div>
    </div>

@endsection