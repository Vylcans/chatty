<nav class="navbar-fixed-top inner-navigation">
    <div class="container-fluid container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                @if(Auth::check())
                    <li class="">
                        <a href="{{ route('home') }}">
                            Sākums
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('user.profile') }}">
                            {{ Auth::user()->getNameOrUsername() }}
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('user.friends') }}">
                            Draugi @if(Auth::user()->pendingFriendRequestCount() > 0) <span class="text-warning">({{ Auth::user()->pendingFriendRequestCount() }})<span> @endif
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('message.get') }}">
                            Vēstules @if(Auth::user()->hasUnreadMessages())<span class="badge">{{ Auth::user()->unreadMessageCount() }}</span>@endif
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('calendar.get') }}">
                            Kalendārs
                        </a>
                    </li>

                    <form class="navbar-form navbar-left" method="post" action="{{ route('user.search') }}" role="search">
                        <div class="form-group">
                            <input type="text" name="search" class="form-control" placeholder="Search">
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>

                    <li class="">
                        <a href="{{ route('auth.logout') }}">
                            Logout
                        </a>
                    </li>
                @else
                    <li class="active">
                        <a href="{{ route('auth.signup') }}">
                            Sign Up <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('auth.signin') }}">
                            Sign In
                        </a>
                    </li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
