<?php

namespace App;


use Facebook\Exceptions\FacebookResponseException;

class Facebook
{
    const SESSION_TOKEN_KEY = 'fb_access_token';

    protected $appId;
    protected $appSecret;
    protected $graphVersion;
    protected $fb;
    protected $error;
    protected $accessToken;
    protected $permissions;
    protected $userFields;

    public function __construct()
    {
        $this->appId = env('FB_APP_ID', null);
        $this->appSecret = env('FB_APP_SECRET', null);
        $this->graphVersion = env('FB_GRAPH_VERSION', null);

        if (!$this->apiParamsSet()) {
            throw new \Exception('Facebook App params not set', 400);
        }

        $this->permissions = [
            'public_profile',
            'user_friends',
            'email',
            'user_photos',
            'user_birthday',
        ];

        $this->fields = [
            'birthday',
            'email',
            'first_name',
            'location',
            'last_name',

        ];

        $this->fb = new \Facebook\Facebook([
            'app_id' => $this->appId,
            'app_secret' => $this->appSecret,
            'default_graph_version' => $this->graphVersion,
        ]);
    }

    public function getError()
    {
        return $this->error;
    }

    public function getAccessToken()
    {
        $sessionAccessToken = \Session::get(self::SESSION_TOKEN_KEY);

        if (!empty($sessionAccessToken)) {
            return $sessionAccessToken;
        }

        return $this->getRedirectLoginAccessToken();
    }

    /**
     * Gets redirect url for authorization
     * @return string
     */
    public function getRedirectLoginUrl()
    {
        $helper = $this->getRedirectLoginHelper();

        return $helper->getLoginUrl(route('facebook.auth.callback'), $this->permissions);
    }

    /**
     * @return null|string
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    public function getRedirectLoginAccessToken()
    {
        $helper = $this->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            $this->error = 'Graph returned an error: ' . $e->getMessage();

            return null;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            $this->error = 'Facebook SDK returned an error: ' . $e->getMessage();

            return null;
        }

        if ($helper->getError()) {
            $this->error = $helper->getError();

            return null;
        }

        //logged in

        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $this->fb->getOAuth2Client();

        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);

        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId($this->appId);

        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');

        $tokenMetadata->validateExpiration();

        if (!$accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                $this->error = "Error getting long-lived access token: " . $helper->getMessage();

                return null;
            }
        }

        $this->accessToken = (string) $accessToken;
        \Session::put(self::SESSION_TOKEN_KEY, $this->accessToken);
        \Session::save();

        return $this->accessToken;
    }

    public function getUser()
    {
        $urlString = '/me';

        if (!empty($this->fields)) {
            $urlString .= '?fields=' . implode(',', $this->fields);
        }

        try {
            // Get the Facebook\GraphNodes\GraphUser object for the current user.
            // If you provided a 'default_access_token', the '{access-token}' is optional.
            $response = $this->fb->get($urlString, $this->getAccessToken());
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            $this->error = 'Graph returned an error: ' . $e->getMessage();

            return null;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            $this->error = 'Facebook SDK returned an error: ' . $e->getMessage();

            return null;
        }

        return $response->getGraphUser();
    }

    public function getImages()
    {
        $user = $this->getUser();

        if (!$user) {
            return null;
        }

        try {
            $response = $this->fb->get('/' . $user->getId() . '/photos?fields=images,picture', $this->getAccessToken());

            $response->decodeBody();

        } catch (FacebookResponseException $e) {
            dd($e->getMessage());
        }

        return $response->getDecodedBody();
    }

    private function getGraphObject($id, $fields = [])
    {
        $url = '/' . $id;
        if (!empty($fields)) {
            if (!is_array($fields)) {
                $fields = [$fields];
            }

            $url .= '?fields=' . implode(',', $fields);
        }

        try {
            $response = $this->fb->get($url, $this->getAccessToken());
            $response->decodeBody();

        } catch (FacebookResponseException $e) {
            dd($e->getMessage());
        }

        return $response;
    }

    public function getImageSource($id)
    {
        $response = $this->getGraphObject($id, 'images');

        $decodedBody = $response->getDecodedBody();

        $link = null;

        if (isset($decodedBody['images']) and !empty($decodedBody['images'])) {
            $firstImage = $decodedBody['images'][0];
            $link = $firstImage['source'];
        }

        return $link;
    }

    /**
     * Checks if required params are set
     *
     * @return bool
     */
    private function apiParamsSet()
    {
        return !is_null($this->appId)
            and
        !is_null($this->appSecret)
            and
        !is_null($this->graphVersion);
    }

    private function getRedirectLoginHelper()
    {
        return $this->fb->getRedirectLoginHelper();
    }

    public function isValidToken($accessToken)
    {
        if (!is_string($accessToken)) {
            return false;
        }

        try {
            $response = $this->fb->get('/debug_token?input_token=' . $accessToken, $accessToken);

        } catch (FacebookResponseException $e) {
            return false;
        }

        $body = $response->getDecodedBody();

        if (isset($body['data']) and isset($body['data']['is_valid'])) {
            return $body['data']['is_valid'];
        }

        return false;
    }
}
