<?php namespace App\Http\Middleware;

use App\Facebook;
use Closure;

class FacebookToken
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();

        $fbId = $user->facebook_id;

        if (!empty($fbId)) {
            $fb = new Facebook();
            if (!$fb->isValidToken($fb->getAccessToken())) {
                return redirect($fb->getRedirectLoginUrl());
            }
        }

        return $next($request);
    }
}
