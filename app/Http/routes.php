<?php

Route::get('/', [
    'uses' => 'HomeController@getIndex',
    'as' => 'home',
]);

Route::post('/signup', [
    'uses' => 'AuthController@postSignUp',
    'as' => 'auth.signup',
]);

Route::get('/logout', [
    'uses' => 'AuthController@getLogout',
    'as' => 'auth.logout',
]);

Route::post('/signin', [
    'uses' => 'AuthController@postSignIn',
    'as' => 'auth.signin',
]);

Route::group(
    [
        'middleware' => ['auth']
    ],
    function () {
        Route::post('/search', [
            'uses' => 'UserController@postSearch',
            'as' => 'user.search',
        ]);

        Route::get('/friends', [
            'uses' => 'UserController@getFriends',
            'as' => 'user.friends',
        ]);

        Route::get('/invite/{id}', [
            'uses' => 'UserController@getInvite',
            'as' => 'user.invite',
        ]);

        Route::get('/accept/{id}', [
            'uses' => 'UserController@getAccept',
            'as' => 'user.accept',
        ]);

        Route::post('/unfriend', [
            'uses' => 'UserController@postUnfriend',
            'as' => 'user.unfriend',
        ]);

        Route::get('/users/{username}', [
            'uses' => 'UserController@getUser',
            'as' => 'user.usersProfile',
        ]);

        Route::post('/status/post', [
            'uses' => 'StatusController@postStatus',
            'as' => 'status.post',
        ]);

        Route::post('/status/{statusId}/reply', [
            'uses' => 'StatusController@postReply',
            'as' => 'status.reply',

        ]);

        Route::get('/status/{statusId}/like', [
            'uses' => 'StatusController@getLike',
            'as' => 'status.like',
        ]);

        Route::get('/calendar', [
            'uses' => 'CalendarController@getCalendar',
            'as' => 'calendar.get',
        ]);

        Route::post('/calendar/set', [
            'uses' => 'CalendarController@postSetCalendarEvent',
            'as' => 'calendar.setEvent',
        ]);

        Route::get('/messages', [
            'uses' => 'MessageController@getMessages',
            'as' => 'message.get',
        ]);

        Route::post('/message/send', [
            'uses' => 'MessageController@postSendMessage',
            'as' => 'message.send',
        ]);

        Route::get('/profile', [
            'uses' => 'UserController@getProfile',
            'as' => 'user.profile',
        ]);

        Route::get('/album', [
            'middleware' => [
                'facebookAccessToken',
            ],
            'uses' => 'UserController@getImages',
            'as' => 'user.images',
        ]);

        Route::post('/profile', [
            'uses' => 'UserController@postProfile',
        ]);

        Route::post('/facebook/download-images', [
            'middleware' => [
                'facebookAccessToken',
            ],
            'uses' => 'FacebookController@postDownloadImages',
            'as' => 'facebook.downloadImages',
        ]);
    }
);

Route::get('/facebook/auth/callback', [
    'uses' => 'FacebookController@getAuthCallback',
    'as' => 'facebook.auth.callback',
]);
