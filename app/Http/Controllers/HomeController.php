<?php

namespace App\Http\Controllers;

use App\Facebook;
use App\Models\Image;
use App\Models\Status;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Action for the home/dashboard view
     *
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        if (\Auth::check()) {

            $user = \Auth::user();

            $statuses = Status::where(function ($query) use ($user) {
                return $query->where('user_id', '=', $user->id)
                    ->orWhereIn('user_id', $user->friends()->lists('id'));
            })
                ->notReply()
                ->orderBy('created_at', 'desc')
                ->paginate(10);

            $friendImages = Image::whereIn('imaginable_id', $user->friends()->lists('id'))
                ->where('imaginable_type', User::FULL_CLASS_NAME)
                ->orderBy('created_at', 'desc')
                ->paginate(10);

            return view(
                'dashboard',
                [
                    'user' => $user,
                    'statuses' => $statuses,
                    'friendImages' => $friendImages,
                ]
            );
        }

        $facebook = new Facebook();

        return view(
            'landingPage.index',
            [
                'excludeNavigation' => true,
                'facebookLoginUrl' => $facebook->getRedirectLoginUrl(),
            ]
        );
    }


}