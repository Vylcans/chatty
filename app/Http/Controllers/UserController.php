<?php

namespace App\Http\Controllers;

use App\Commands\UploadUserAvatarCommand;
use App\Events\FriendRequestSent;
use App\Events\FriendshipAcceptedEvent;
use App\Facebook;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Search for users
     *
     * @return \Illuminate\View\View
     */
    public function postSearch()
    {
        $search = \Input::get('search');

        if (!$search) {
            $search = '';
        }

        return view('search', [
            'result' => User::findUsers($search),
            'query' => $search
        ]);
    }

    /**
     * Opens users profile by username
     *
     * @param $username
     * @return \Illuminate\View\View
     */
    public function getUser($username)
    {
        $user = User::where('username', '=', $username)->first();

        if (!$user) {
            \App::abort(404);
        }

        $statuses = $user->statuses()->notReply()->get();

        return view(
            'user',
            [
                'user' => $user,
                'statuses' => $statuses,
                'authUserIsFriend' => \Auth::user()->isFriendWith($user)
            ]
        );
    }

    /**
     * Users friends view
     *
     * @return \Illuminate\View\View
     */
    public function getFriends()
    {
        return view(
            'friends',
            [
                'friends' => \Auth::user()->friends(),
                'friendRequestsCreated' => \Auth::user()->friendRequestsCreated(),
                'friendRequestsReceived' => \Auth::user()->friendRequestsReceived(),
            ]
        );
    }

    /**
     * Accepts friend request
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getAccept($id)
    {
        $user = User::find($id);
        if (!$user) {
            \App::abort(404);
        }

        \Auth::user()->acceptFriendRequest($user);

        /**
         * Event creates status entry about confirmed friendship
         */
        \Event::fire(new FriendshipAcceptedEvent(\Auth::user(), $user));

        return redirect()->back();
    }

    /**
     * Invites a friend
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getInvite($id)
    {
        $userToAdd = User::find($id);
        if (!$userToAdd) {
            \App::abort(404);
        }

        $currentUser = \Auth::user();

        $currentUser->addFriend($userToAdd);

        //let's trigger the event to send an email
        \Event::fire(new FriendRequestSent($currentUser, $userToAdd));

        return redirect()->back();
    }

    /**
     * Unfriends a friend
     *
     * @param \Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUnfriend(\Request $request)
    {
        //form used for csrf protection

        $friend = User::find(\Input::get('userId'));

        if (!$friend) {
            \App::abort(404, 'friend not found');
        }

        if (!\Auth::user()->isFriendWith($friend)) {
            \App::abort(404, 'you are no friends');
        }

        \Auth::user()->deleteFriend($friend);

        return redirect()->back();
    }

    /**
     * Gets logged in user's profile
     *
     * @return \Illuminate\View\View
     */
    public function getProfile()
    {
        $user = \Auth::user();

        $facebookLoginUrl = null;

        if (is_null($user->facebook_id)) {
            $facebook = new Facebook();
            $facebookLoginUrl = $facebook->getRedirectLoginUrl();
        }

        return view(
            'profile',
            [
                'user' => $user,
                'facebookLoginUrl' => $facebookLoginUrl,
            ]
        );
    }

    /**
     * Edits logged in user's profile
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postProfile(Request $request)
    {
        $this->validate(
            $request,
            [
                'firstName' => 'required|min:2|max:100',
                'lastName' => 'required|min:2|max:100',
                'username' => 'required|min:2|max:100|unique:users,username,'.\Auth::user()->id,
                'email' => 'required|min:2|max:100|email|unique:users,email,'.\Auth::user()->id,
                'avatar' => 'mimes:jpeg,bmp,png',
            ]
        );

        $extras = [
            'user' => \Auth::user(),
        ];

        $this->dispatchFrom('App\Commands\UpdateUserProfileCommand', $request, $extras);

        if ($request->hasFile('avatar')) {
            $this->dispatch(new UploadUserAvatarCommand(\Auth::user(), $request->file('avatar')));
        }

        return redirect()->back();
    }

    public function getImages()
    {
        /**
         * @var User $user
         */
        $user = \Auth::user();

        $images = $user->images;
        $facebookImages = [];

        if ($user->facebook_id) {
            $facebook = new Facebook();

            $retrievedImages = $facebook->getImages();

            $retrievedImages = $retrievedImages['data'];

            foreach ($retrievedImages as $image) {
                $image['isLocal'] = false;
                $facebookImages[] = $image;
            }
        }

        return view(
            'userImages',
            [
                'images' => $images,
                'facebookImages' => $facebookImages,
            ]
        );
    }
}
