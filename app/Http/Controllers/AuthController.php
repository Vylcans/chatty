<?php
/**
 * Created by PhpStorm.
 * User: vylcans
 * Date: 16.19.1
 * Time: 15:30
 */

namespace App\Http\Controllers;

use App\Commands\CreateUserCommand;
use App\Events\UserLoginEvent;
use App\Events\UserSignupEvent;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Sign Up action
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSignUp(Request $request)
    {
        $this->validate($request, [
            'firstName' => 'min:2|max:20|alpha_dash',
            'lastName' => 'min:2|max:20|alpha_dash',
            'username' => 'required|min:3|max:20|alpha_dash|unique:users',
            'email' => 'required|min:3|email|unique:users',
            'password' => 'required|min:5',
        ]);

        /**
         * dispatchFrom - Laravel will auto-map the keys on that array or arrayAccessible object to the
         * same property names in your command constructor.
         */
        $this->dispatchFrom('App\Commands\CreateAndAuthorizeUserCommand', $request);

        return \Redirect::route('home');
    }

    /**
     * Sign In action
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSignIn(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        if (!\Auth::attempt($request->only(['email', 'password']))) {
            return redirect()->back()->with('info', 'Could not sign in');
        }

        \Event::fire(new UserLoginEvent(\Auth::user()));

        return \Redirect::route('home');
    }

    /**
     * Log Out action
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogout()
    {
        \Auth::logout();

        \Session::clear();

        return \Redirect::route('home');
    }
}
