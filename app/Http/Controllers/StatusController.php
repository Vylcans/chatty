<?php

namespace App\Http\Controllers;

use App\Commands\CreatePostCommand;
use App\Models\Status;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    /**
     * Posts time line status
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postStatus(Request $request)
    {
        $this->validate($request, [
            'status' => 'required|max:1000'
        ]);

        $this->dispatch(new CreatePostCommand(\Auth::user(), $request->status));

        return redirect()->back();
    }

    /**
     * Replies to the time line post
     *
     * @param Request $request
     * @param $statusId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postReply(Request $request, $statusId)
    {
        $this->validate($request, [
            'reply-'.$statusId => 'required|max:1000',
        ], [
            'required' => 'The reply body is required.'
        ]);

        $currentUser = \Auth::user();

        $status = Status::notReply()->find($statusId);

        if (!$status) {
            \App::abort(404);
        }

        if (!$currentUser->isFriendWith($status->user) and $currentUser->id !== $status->user->id) {
            //if tries to reply unknown person's status and not it's own
            \App::abort(404, 'Cannot reply to unknown/yourself');
        }

        $replyBody = \Input::get('reply-' . $statusId);

        $this->dispatch(new CreatePostCommand($currentUser, $replyBody, $status));

        return redirect()->back();
    }

    /**
     * Likes a time line post (or reply)
     *
     * @param $statusId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLike($statusId)
    {
        $status = Status::find($statusId);

        if (!$status) {
            \App::abort(404, 'Status not found');
        }

        if (!\Auth::user()->isFriendWith($status->user) or \Auth::user()->id == $status->user->id) {
            //cannot like not-users status or tries to like it's own status
            \App::abort(404, 'Cannot like unknown/yourself');
        }

        if (\Auth::user()->hasLikedStatus($status)) {
            \App::abort(404, 'Already liked');
        }

        $like = $status->likes()->create([]);

        \Auth::user()->likes()->save($like);

        return redirect()->back();
    }
}
