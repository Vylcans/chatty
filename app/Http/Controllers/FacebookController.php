<?php

namespace App\Http\Controllers;

use App\Commands\SaveImagesFromSourceCommand;
use App\Facebook;
use App\Models\Image;
use App\Models\User;
use Illuminate\Http\Request;

class FacebookController extends Controller
{
    public function getAuthCallback()
    {
        $facebook = new Facebook();
        $accessToken = $facebook->getAccessToken();

        if (is_null($accessToken)) {
            throw new \Exception($facebook->getError());
        }

        $facebookUser = $facebook->getUser();
        $facebookId = $facebookUser->getId();

        if (is_null($facebookUser)) {
            throw new \Exception($facebook->getError());
        }

        if (\Auth::check()) {
            $currentUser = \Auth::user();

            if ($currentUser->facebook_id) {
                //after expired token
                return redirect(route('home'));
            }

            $currentUser->update([
                'facebook_id' => $facebookId,
            ]);

            return redirect(route('user.profile'));
        }

        //callback from registration/log in
        $userByFacebookId = User::where('facebook_id', '=', $facebookId)->first();

        //existing user login
        if ($userByFacebookId) {
            \Auth::login($userByFacebookId);

            return redirect(route('home'));
        }

        $userData = [
            'firstName' => $facebookUser->getFirstName(),
            'lastName' => $facebookUser->getLastName(),
            'username' => 'user_' . rand() . time(),
            'email' => null,
            'password' => rand(), //since password cannot be null
            'facebookId' => $facebookUser->getId(),
        ];

        $this->dispatchFromArray('App\Commands\CreateAndAuthorizeUserCommand', $userData);

        return redirect(route('home'));
    }

    public function postDownloadImages(Request $request)
    {
        $images = $request->images;

        $fb = new Facebook();

        $imageSourcesToDownload = [];

        foreach ($images as $imageId) {
            $source = $fb->getImageSource($imageId);
            if ($source) {
                $imageSourcesToDownload[] = $source;
            }
        }

        if (!empty($imageSourcesToDownload)) {
            $targetPath = public_path('images/' . Image::DIRECTORY_USER_IMAGES);
            $this->dispatch(new SaveImagesFromSourceCommand(\Auth::user(), $imageSourcesToDownload, $targetPath));
        }

        return \Response::json([]);
    }
}
