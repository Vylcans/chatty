<?php

namespace App\Http\Controllers;

use App\Models\CalendarEvent;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    /**
     * Returns calendar view
     *
     * @return \Illuminate\View\View
     */
    public function getCalendar()
    {
        $events = [];

        $userEvents = \Auth::user()->calendarEvents;

        /**
         * @var CalendarEvent $event
         */
        foreach ($userEvents as $event) {
            $events[] = \Calendar::event(
                $event->getTitle(), //event title
                false, //full day event?
                $event->getStart(), //start time (you can also use Carbon instead of DateTime)
                $event->getEnd(), //end time (you can also use Carbon instead of DateTime)
                0 //optionally, you can specify an event ID
            );
        }

        $calendar = \Calendar::addEvents($events) //add an array with addEvents
            ->setOptions([ //set fullcalendar options
                'firstDay' => 1
            ])
            /*
            ->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
                'viewRender' => 'function() {alert("Callbacks!");}'
            ])
            */
            ;

        return view('calendar', compact('calendar'));
    }

    /**
     * Saves calendar event
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSetCalendarEvent(Request $request)
    {
        //todo some validation

        $start = \DateTime::createFromFormat('d.m.Y H:i', \Input::get('start'));
        $end = \DateTime::createFromFormat('d.m.Y H:i', \Input::get('end'));
        $title = \Input::get('title');

        if ($start and $end and !empty($title)) {
            $event = CalendarEvent::create([
                'start' => $start,
                'end' => $end,
                'title' => $title
            ]);

            \Auth::user()->calendarEvents()->save($event);
        }

        return redirect()->back();
    }
}