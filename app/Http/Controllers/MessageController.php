<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MessageController extends Controller
{

    /**
     * Gets messages view
     *
     * @return \Illuminate\View\View
     */
    public function getMessages()
    {
        $messages = \Auth::user()->receivedMessages()->orderBy('id', 'desc')->get();

        foreach ($messages->where('seen', '0') as $message) {
            $message->update([
                'seen' => true,
                'seen_at' => new \DateTime(),
            ]);

            $message->new = true;
        }

        return view(
            'messages',
            [
                'messages' => $messages,
            ]
        );
    }

    /**
     * Sends a message
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSendMessage(Request $request)
    {
        $this->validate(
            $request,
            [
                'body' => 'required|max:1000',
                'recipientId' => 'required|exists:users,id',
            ]
        );

        $this->dispatchFrom('App\Commands\SendMessageCommand', $request);

        return redirect()->back();
    }
}
