<?php namespace App\Providers;

use App\Models\User;
use App\Observers\UserObserver;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Log;

class EventServiceProvider extends ServiceProvider
{

    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'event.name' => [
            'EventListener',
        ],
        'App\Events\UserCreatedEvent' => [
            'App\Handlers\Events\CreateNewUserSubscription',
            'App\Handlers\Events\SendRegisteredUserEmail',
        ],
        'App\Events\FriendRequestSent' => [
            'App\Handlers\Events\EmailFriendRequest'
        ],
        'App\Events\UserLoginEvent' => [
            'App\Handlers\Events\UserLoginHandler'
        ],
        'App\Events\FriendshipAcceptedEvent' => [
            'App\Handlers\Events\FriendshipAcceptedHandler'
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //as soon as the event is being triggered, this happens also. $event is the actual instance of the event
        \Event::listen('App\Events\FriendRequestSent', function ($event) {
            Log::debug('EventServiceProvider::boot');
        });

        /**
         * Observes actions on model and triggers responses
         */
        User::observe(new UserObserver());
    }
}
