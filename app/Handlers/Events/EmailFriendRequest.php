<?php namespace App\Handlers\Events;

use App\Events\FriendRequestSent;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Support\Facades\Log;

class EmailFriendRequest
{
    /**
     * Handle the event.
     *
     * @param  FriendRequestSent  $event
     * @return void
     */
    public function handle(FriendRequestSent $event)
    {
        $to = $event->getInvitedUserEmail();
        $invitedPersonName = $event->getInvitedUserNameOrUsername();
        $invitingPersonName = $event->getInvitingUserNameOrUsername();

        $mailData = [
            'invitedPerson' => $invitedPersonName,
            'invitingPerson' => $invitingPersonName
        ];

        \Mail::send('emails.friendRequest', $mailData, function ($message) use ($to, $invitedPersonName) {
            $message->to($to, $invitedPersonName)->subject('New friend request!');
        });
    }
}
