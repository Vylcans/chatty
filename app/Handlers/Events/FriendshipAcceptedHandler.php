<?php namespace App\Handlers\Events;

use App\Events\FriendshipAcceptedEvent;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class FriendshipAcceptedHandler {

    /**
     * Handle the event.
     *
     * @param  FriendshipAcceptedEvent  $event
     * @return void
     */
    public function handle(FriendshipAcceptedEvent $event)
    {
        $personThatAccepted = $event->getPersonThatAccepted();

        $statusBody = $personThatAccepted->getNameOrUsername() .
            ' ir draugos ar ' . $event->getAcceptedPersonName() . '!';

        $event->getPersonThatAccepted()
            ->statuses()
            ->create([
                'body' => $statusBody
            ]);
    }

}
