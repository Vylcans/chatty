<?php namespace App\Handlers\Events;

use App\Events\UserLoginEvent;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class UserLoginHandler
{

    /**
     * Handle the event.
     *
     * @param  UserLoginEvent  $event
     * @return void
     */
    public function handle(UserLoginEvent $event)
    {
        $user = $event->getUser();

        $user->last_login = new \DateTime();
        $user->save();
    }
}
