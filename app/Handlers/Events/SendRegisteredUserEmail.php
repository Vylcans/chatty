<?php namespace App\Handlers\Events;

use App\Events\UserCreatedEvent;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class SendRegisteredUserEmail
{

    /**
     * Handle the event.
     *
     * @param  UserCreatedEvent  $event
     * @return void
     */
    public function handle(UserCreatedEvent $event)
    {
        $email = $event->getEmail();

        if ($email) {
            $password = $event->getPassword();

            $personName = $event->getUserName();
            $mailData = [
                'password' => $password,
                'nameOrUsername' => $personName,
                'email' => $email,
            ];

            \Mail::send('emails.signup', $mailData, function ($message) use ($email, $personName) {
                $message->to($email, $personName)->subject('Signup to chatty!');
            });
        }
    }
}
