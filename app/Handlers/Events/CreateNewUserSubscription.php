<?php namespace App\Handlers\Events;

use App\Events\UserCreatedEvent;

use App\Models\Newsletter;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class CreateNewUserSubscription
{

    /**
     * Handle the event.
     *
     * @param  UserCreatedEvent  $event
     * @return void
     */
    public function handle(UserCreatedEvent $event)
    {
        $email = $event->getEmail();

        if ($email) {
            Newsletter::create([
                'email' => $email
            ]);
        }
    }
}
