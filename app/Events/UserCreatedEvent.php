<?php namespace App\Events;

use App\Events\Event;

use App\Models\User;
use Illuminate\Queue\SerializesModels;

class UserCreatedEvent extends Event
{

    use SerializesModels;

    private $user;
    private $userName;
    private $email;
    private $password;

    /**
     * @param User $user
     * @param string $password
     */
    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->email = $user->email;
        $this->password = $password;
        $this->userName = $user->getNameOrUsername();
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getUserName()
    {
        return $this->userName;
    }
}
