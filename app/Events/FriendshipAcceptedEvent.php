<?php namespace App\Events;

use App\Events\Event;

use App\Models\User;
use Illuminate\Queue\SerializesModels;

class FriendshipAcceptedEvent extends Event
{

    use SerializesModels;

    private $personThatAccepted;
    private $personToAccept;

    /**
     * @param User $personThatAccepted
     * @param User $personToAccept
     */
    public function __construct(User $personThatAccepted, User $personToAccept)
    {
        $this->personThatAccepted = $personThatAccepted;
        $this->personToAccept = $personToAccept;
    }

    public function getPersonThatAccepted()
    {
        return $this->personThatAccepted;
    }

    public function getAcceptedPersonName()
    {
        return $this->personToAccept->getNameOrUsername();
    }
}
