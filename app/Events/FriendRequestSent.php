<?php namespace App\Events;

use App\Events\Event;

use App\Models\User;
use Illuminate\Queue\SerializesModels;

/**
 * Class FriendRequestSent
 *
 * Handler App\Handlers\Events\EmailFriendRequest
 *
 * @package App\Events
 */
class FriendRequestSent extends Event
{

    use SerializesModels;

    private $invitingUser;
    private $userToInvite;

    /**
     * @param User $invitingUser
     * @param User $userToInvite
     */
    public function __construct(User $invitingUser, User $userToInvite)
    {
        $this->invitingUser = $invitingUser;
        $this->userToInvite = $userToInvite;
    }

    public function getInvitingUserNameOrUsername()
    {
        return $this->invitingUser->getNameOrUsername();
    }

    public function getInvitedUserNameOrUsername()
    {
        return $this->userToInvite->getNameOrUsername();
    }

    public function getInvitedUserEmail()
    {
        return $this->userToInvite->email;
    }

}
