<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User extends Model implements AuthenticatableContract, ImaginableInterface
{

    use Authenticatable;

    const AVATAR_DIRECTORY = 'avatars';
    const AVATAR_EXTENSION = '.jpg';
    const AVATAR_THUMBNAIL_PREFIX = 'small_';
    const FULL_CLASS_NAME = __CLASS__;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'location',
        'username',
        'email',
        'password',
        'avatar',
        'facebook_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function statuses()
    {
        return $this->hasMany('App\Models\Status', 'user_id');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imaginable');
    }

    public function getName()
    {
        if ($this->first_name and $this->last_name) {
            return $this->first_name . ' ' . $this->last_name;
        }

        if ($this->first_name) {
            return $this->first_name;
        }

        return null;
    }


    public function getNameOrUsername()
    {
        return $this->getName() ?: $this->username;
    }

    /**
     * People whom current user has added as friends
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function friendsOfMine()
    {
        return $this->belongsToMany('App\Models\User', 'friends', 'user_id', 'friend_id');
    }

    /**
     * People who has added current user to their friend list
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function friendOf()
    {
        return $this->belongsToMany('App\Models\User', 'friends', 'friend_id', 'user_id');
    }

    public function friends()
    {
        return $this->friendsOfMine()
            ->wherePivot('accepted', '=', 1)
            ->get()
            ->merge(
                $this->friendOf()
                    ->wherePivot('accepted', '=', 1)
                    ->get()
            );
    }

    public function isFriendWith(User $user)
    {
        return (bool) $this->friends()->where('id', $user->id)->count();
    }

    /**
     * Friend requests made by the current user
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function friendRequestsCreated()
    {
        return $this->friendsOfMine()->wherePivot('accepted', false)->get();
    }

    /**
     * Friend requests pending for the current user from other users
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function friendRequestsReceived()
    {
        return $this->friendOf()->wherePivot('accepted', false)->get();
    }

    public function pendingFriendRequestCount()
    {
        return count($this->friendRequestsReceived());
    }

    /**
     * Checks if current user has created friend request that is pending for the given user
     * @param User $user
     * @return bool
     */
    public function hasPendingFriendRequestsCreated(User $user)
    {
        return (bool) $this->friendRequestsCreated()->where('id', $user->id)->count();
    }

    /**
     * Checks if current user has pending request FROM the given user
     * @param User $user
     * @return bool
     */
    public function hasPendingReceivedFriendRequest(User $user)
    {
        return (bool) $this->friendRequestsReceived()->where('id', $user->id)->count();
    }

    public function addFriend(User $user)
    {
        $this->friendsOfMine()->attach($user->id);
    }

    public function deleteFriend(User $user)
    {
        $this->friendsOfMine()->detach($user->id);
    }

    public function acceptFriendRequest(User $user)
    {
        $this->friendRequestsReceived()->where('id', $user->id)->first()->pivot->update(['accepted' => true]);
    }

    public function sentMessages()
    {
        return $this->hasMany('App\Models\Message', 'sender_id');
    }

    public function receivedMessages()
    {
        return $this->hasMany('App\Models\Message', 'recipient_id');
    }

    public function unreadMessageCount()
    {
        return $this->receivedMessages()->where('seen', false)->count();
    }

    public function hasUnreadMessages()
    {
        return $this->unreadMessageCount() > 0;
    }

    /**
     * Searches for the user
     * @param $searchString
     * @return mixed
     */
    public static function findUsers($searchString)
    {
        $currentUser = \Auth::user();

        $query = User::where(function ($query) use ($searchString) {
            $query->where('username', 'like', '%' . $searchString . '%')
                ->orWhere(\DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', '%' . $searchString . '%')
                ->orWhere(\DB::raw("CONCAT(`last_name`, ' ', `first_name`)"), 'LIKE', '%' . $searchString . '%');
        });

        if ($currentUser) {
            $query->where('id', '!=', $currentUser->id);
        }

        return $query->get();
    }

    public function getAvatarUrl()
    {
        $avatar = $this->getAvatarFile(self::AVATAR_THUMBNAIL_PREFIX);
        if (is_file($avatar)) {
            return $this->getAvatarLink(self::AVATAR_THUMBNAIL_PREFIX);
        }

        return 'https://www.gravatar.com/avatar/' . md5($this->email) . '?d=mm&s=40';
    }

    public function hasLikedStatus(Status $status)
    {
        return (bool) $status->likes
            ->where('user_id', $this->id)
            ->count();
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Like');
    }

    public function chatMessages()
    {
        return $this->hasMany('App\Models\ChatMessage');
    }

    public function calendarEvents()
    {
        return $this->hasMany('App\Models\CalendarEvent');
    }

    private function getAvatarDirectory()
    {
        return public_path('images/' . self::AVATAR_DIRECTORY) . '/';
    }

    public function getAvatarFile($prefix = '')
    {
        return $this->getAvatarDirectory() . $prefix . $this->avatar;
    }

    public function getAvatarFileByFilename($filename)
    {
        return $this->getAvatarDirectory() . $filename;
    }

    public function getAvatarLink($prefix = '')
    {
        return asset('images/' . self::AVATAR_DIRECTORY . '/' . $prefix . $this->avatar);
    }

    public function generateAvatarFilename()
    {
        return $this->username . '_' . time() . self::AVATAR_EXTENSION;
    }

    public function deleteAvatar()
    {
        $avatar = $this->getAvatarFile();
        $avatarThumbnail = $this->getAvatarFile(self::AVATAR_THUMBNAIL_PREFIX);

        if (is_file($avatar)) {
            unlink($avatar);
        }

        if (is_file($avatarThumbnail)) {
            unlink($avatarThumbnail);
        }
    }
}
