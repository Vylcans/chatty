<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'chat_messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'body',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}