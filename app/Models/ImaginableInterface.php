<?php

namespace App\Models;

interface ImaginableInterface
{
    public function images();
}
