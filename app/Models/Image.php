<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

    const THUMBNAIL_PREFIX = 'sm_';
    const DIRECTORY_USER_IMAGES = 'user-images';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'imaginable';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'imaginable_id',
        'imaginable_type',
        'source',
    ];

    public function imaginable()
    {
        return $this->morphTo();
    }

    public function getUserImageSource()
    {
        return asset('images/' . self::DIRECTORY_USER_IMAGES . '/' . $this->source);
    }
}
