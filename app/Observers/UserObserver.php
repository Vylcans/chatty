<?php

namespace App\Observers;

use App\Models\User;

/**
 * Class UserObserver
 * @package App\Observers
 *
 * Possible model events:
 * creating(), created(), updating(), updated(), saving(), saved(), deleting(), deleted(), restoring(), restored()
 */
class UserObserver
{
    /**
     * Action that happens whenever model User is being created
     *
     * @param User $user
     */
    public function created(User $user)
    {
        $date = new \DateTime();

        $user->last_login = $date;
        $user->signup_date = $date;
        $user->save();
    }
}
