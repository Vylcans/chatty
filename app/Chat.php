<?php

namespace App;

use App\Models\User;
use App;
use Auth;
use Config;
use Crypt;
use Illuminate\Session\SessionManager;
use Ratchet\MessageComponentInterface;
use SplObjectStorage;

class Chat implements MessageComponentInterface
{
    protected $clients;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(\Ratchet\ConnectionInterface $conn)
    {

        // Bind the session handler to the client connection
        $session = $this->getCurrentSession($conn);
        $conn->session = $session;

        $this->clients->attach($conn);

        $lastMessages = App\Models\ChatMessage::limit(10)->get();

        foreach ($lastMessages as $message) {

            $name = 'Anonymous';

            if ($message->user instanceof User) {
                $name = $message->user->getNameOrUsername();
            }

            $response = $this->renderMessageData($message->body, $name, $message->created_at->diffForHumans());

            $conn->send($response);
        }

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(\Ratchet\ConnectionInterface $conn, $msg)
    {

        $currentUser = $this->getCurrentUser($conn);
        $name = 'Anonymous';

        $numRecv = count($this->clients) - 1;
        echo sprintf(
            'Connection %d sending message "%s" to %d other connection%s' . "\n",
            $conn->resourceId,
            $msg,
            $numRecv,
            $numRecv == 1 ? '' : 's'
        );

        $msg = json_decode($msg);

        $message = App\Models\ChatMessage::create([
            'body' => $msg->msg,
        ]);

        if ($currentUser instanceof User) {
            $name = $currentUser->getNameOrUsername();
            $currentUser->chatMessages()->save($message);
        }

        $response = $this->renderMessageData($msg->msg, $name, $message->created_at->diffForHumans());

        foreach ($this->clients as $client) {
            //if ($conn !== $client) {
                // The sender is not the receiver, send to each client connected
                $client->send($response);
            //}
        }
    }

    public function onClose(\Ratchet\ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(\Ratchet\ConnectionInterface $conn, \Exception $exception)
    {
        echo "Error occurred: " . $exception->getMessage();

        $conn->close();
    }

    private function getCurrentSession(\Ratchet\ConnectionInterface $conn)
    {
        // Create a new session handler for this client
        $session = (new SessionManager(App::getInstance()))->driver();
        // Get the cookies
        $cookies = $conn->WebSocket->request->getCookies();
        // Get the laravel's one
        $laravelCookie = urldecode($cookies[Config::get('session.cookie')]);
        // get the user session id from it
        $idSession = Crypt::decrypt($laravelCookie);
        // Set the session id to the session handler
        $session->setId($idSession);

        return $session;
    }

    private function getCurrentUser(\Ratchet\ConnectionInterface $conn)
    {
        $currentUser = null;
        // start the session when the user send a message
        // (refreshing it to be sure that we have access to the current state of the session)
        $conn->session->start();
        // do what you wants with the session
        // for example you can test if the user is auth and get his id back like this:
        $idUser = $conn->session->get(Auth::getName());

        if (isset($idUser)) {
            $currentUser = User::find($idUser);
        }

        // or you can save data to the session
        $conn->session->put('foo', 'bar');
        // ...
        // and at the end. save the session state to the store
        $conn->session->save();

        return $currentUser;
    }

    private function renderMessageData($messageText, $name, $time)
    {
        $response = new \stdClass();
        $response->msg = view(
            'chatResponse',
            [
                'message' => $messageText,
                'author' => $name,
                'time' => $time,
            ]
        )
            ->render();

        return json_encode($response);
    }
}