<?php namespace App\Commands;

use App\Commands\Command;

use App\Models\Image;
use App\Models\ImaginableInterface;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Intervention\Image\Facades\Image as InterventionImage;

class SaveImagesFromSourceCommand extends Command implements SelfHandling, ShouldBeQueued
{

    private $sources;
    private $parent;
    private $targetPath;

    public function __construct(ImaginableInterface $parent, $sources, $targetPath)
    {
        if (!is_array($sources)) {
            $sources = [$sources];
        }

        $this->sources = $sources;
        $this->parent = $parent;
        $this->targetPath = $targetPath;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->sources as $source) {
            $filename = md5($source) . '.jpg';
            $target = $this->targetPath . '/' . $filename;

            try {
                InterventionImage::make($source)
                    ->save($target);
            } catch (FileException $e) {
                \App::abort(400, $e->getMessage());
            }

            $image = Image::create([
                'source' => $filename,
            ]);
            $this->parent->images()->save($image);
        }
    }
}
