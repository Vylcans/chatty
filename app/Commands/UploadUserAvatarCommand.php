<?php namespace App\Commands;

use App\Commands\Command;

use App\Models\User;
use Illuminate\Contracts\Bus\SelfHandling;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Intervention\Image\Facades\Image;

class UploadUserAvatarCommand extends Command implements SelfHandling
{

    protected $user;
    protected $uploadedFile;

    public function __construct(User $user, UploadedFile $uploadedFile)
    {
        $this->user = $user;
        $this->uploadedFile = $uploadedFile;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $avatar = null;
        $avatarThumbnail = null;

        $avatarFilename = $this->user->generateAvatarFilename();
        $avatarThumbnailFilename = User::AVATAR_THUMBNAIL_PREFIX . $avatarFilename;
        $avatarDestinationFile = $this->user->getAvatarFileByFilename($avatarFilename);
        $avatarThumbnailDestinationFile = $this->user->getAvatarFileByFilename($avatarThumbnailFilename);

        try {
            Image::make($this->uploadedFile->getRealPath())
                ->fit(500, 500)
                ->save($avatarDestinationFile);

            Image::make($this->uploadedFile->getRealPath())
                ->fit(50, 50)
                ->save($avatarThumbnailDestinationFile);

        } catch (FileException $e) {
            \App::abort(400, $e->getMessage());
        }

        $this->user->deleteAvatar();
        $this->user->update([
            'avatar' => $avatarFilename,
        ]);
    }
}
