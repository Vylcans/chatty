<?php namespace App\Commands;

use App\Commands\Command;

use App\Models\Message;
use App\Models\User;
use Illuminate\Contracts\Bus\SelfHandling;

class SendMessageCommand extends Command implements SelfHandling
{
    /**
     * @var User
     */
    protected $recipient;
    /**
     * @var User
     */
    protected $sender;
    protected $body;

    /**
     * @param $body
     * @param $recipientId
     */
    public function __construct($body, $recipientId)
    {
        $this->recipient = User::find($recipientId);
        $this->sender = \Auth::user();
        $this->body = $body;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $message = Message::create([
            'body' => $this->body,
        ]);

        $this->sender->sentMessages()->save($message);
        $this->recipient->receivedMessages()->save($message);
    }
}
