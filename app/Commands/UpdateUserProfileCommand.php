<?php namespace App\Commands;

use App\Commands\Command;

use App\Models\User;
use Illuminate\Contracts\Bus\SelfHandling;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UpdateUserProfileCommand extends Command implements SelfHandling
{

    protected $user;
    protected $firstName;
    protected $lastName;
    protected $username;
    protected $email;

    /**
     * @param User $user
     * @param string $firstName
     * @param string $lastName
     * @param string $username
     * @param string $email
     */
    public function __construct(User $user, $firstName, $lastName, $username, $email)
    {
        $this->user = $user;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->username = $username;
        $this->email = $email;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $update = [];

        if ($this->user->first_name != $this->firstName) {
            $update['first_name'] = $this->firstName;
        }

        if ($this->user->last_name != $this->lastName) {
            $update['last_name'] = $this->lastName;
        }

        if ($this->user->username != $this->username) {
            $update['username'] = $this->username;
        }

        if ($this->user->email != $this->email) {
            $update['email'] = $this->email;
        }

        if (!empty($update)) {
            $this->user->update($update);
        }
    }
}
