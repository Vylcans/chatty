<?php namespace App\Commands;

use App\Commands\Command;

use App\Events\UserCreatedEvent;
use App\Models\User;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateAndAuthorizeUserCommand extends Command implements SelfHandling
{

    private $firstName;
    private $lastName;
    private $username;
    private $email;
    private $password;
    private $facebookId;

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string $username
     * @param string $email
     * @param string $password
     * @param null|string $facebookId
     */
    public function __construct($firstName, $lastName, $username, $email, $password, $facebookId = null)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
        $this->facebookId = $facebookId;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::create([
            'first_name' => empty($this->firstName) ? null : $this->firstName,
            'last_name' => empty($this->lastName) ? null : $this->lastName,
            'username' => $this->username,
            'email' => $this->email,
            'password' => \Hash::make($this->password),
            'facebook_id' => $this->facebookId,
        ]);

        \Event::fire(new UserCreatedEvent($user, $this->password));

        \Auth::login($user);
    }
}
