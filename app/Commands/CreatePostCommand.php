<?php namespace App\Commands;

use App\Commands\Command;

use App\Models\Status;
use App\Models\User;
use Illuminate\Contracts\Bus\SelfHandling;

class CreatePostCommand extends Command implements SelfHandling
{

    private $author;
    private $body;
    private $parentStatus;

    public function __construct(User $author, $body, Status $parentStatus = null)
    {
        $this->author = $author;
        $this->body = $body;
        $this->parentStatus = $parentStatus;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $status = Status::create([
            'body' => $this->body
        ]);

        $this->author->statuses()->save($status);

        if ($this->parentStatus instanceof Status) {
            $this->parentStatus->replies()->save($status);
        }
    }
}
